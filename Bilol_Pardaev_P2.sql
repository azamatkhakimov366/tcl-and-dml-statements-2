UPDATE film 
SET rental_duration = 21,
 	rental_rate = 9.99
	where film_id = 2000
	
WITH updatedCustomer AS (
    SELECT py.customer_id
    FROM payment py
    JOIN rental rn ON py.rental_id = rn.rental_id
    GROUP BY py.customer_id, rn.customer_id
    HAVING COUNT(*) > 10
    LIMIT 1
)
UPDATE customer cm
SET first_name = 'Sirojiddin',
	last_name = 'Mixritdinov',
	email = 'sirojiddinmexriddinov4@gmail.com',
	store_id = 2,
	address_id = 4,
	create_date = current_date
FROM updatedCustomer
WHERE cm.customer_id = updatedCustomer.customer_id;


-- This is for checking if the selected customer details has been changed or not

SELECT cr.*
FROM customer cr
JOIN (
    SELECT py.customer_id
    FROM payment py
    JOIN rental rn ON py.rental_id = rn.rental_id
    GROUP BY py.customer_id
    HAVING COUNT(*) > 10
    LIMIT 1
) AS ucr
ON cr.customer_id = ucr.customer_id;
